#!/bin/bash

if [[ -z "${EXECUTE_HOME}" ]]; then
  file="/execute"
else
  file="${EXECUTE_HOME}"
fi

echo "EXECUTE_HOME : $file"

if [ -d "$file" ]
then
        echo "$file path exist...start to execute"
        chmod 755 $file/main
        $file/main
else
        echo "$file not found"
fi
